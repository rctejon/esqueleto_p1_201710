package model.logic;

import java.io.*;
import java.util.*;

import api.ISistemaRecomendacionPeliculas;
import model.data_structures.*;
import model.data_structures.Stack;
import model.vo.*;
import model.data_structures.Queue;
import model.vo.*;


public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas
{
	private ILista<VOPelicula> misPeliculas;
	private ILista<VORating> misRatings;
	private ILista<VOTag> misTags;
	private ILista<VOUsuario> usuarios;
	private Stack<VOOperacion> misOperaciones;
	private ILista<VOGeneroPelicula> listaPeliculaGenero;
	private Queue<VOPeliculaRating> cola;
	private Queue<VOPelicula> colaPelicula;

	private ILista<VOGenero> generos;

	public SistemaRecomendacionPeliculas()
	{
		misPeliculas = new ListaDobleEncadenada<VOPelicula>();
		misRatings = new ListaDobleEncadenada<VORating>();
		misTags = new ListaDobleEncadenada<VOTag>();
		usuarios = new ListaDobleEncadenada<VOUsuario>();
		misOperaciones = new Stack<VOOperacion>();
		generos = new ListaDobleEncadenada<VOGenero>();   
		cola = new Queue<VOPeliculaRating>();
	}

	private VOPelicula nuevaPelicula(String line) {

		VOPelicula film = new VOPelicula();
		int last = line.lastIndexOf(",");
		String com = line.substring(last+1);

		ListaEncadenada<String> gen = new ListaEncadenada<>();

		String[] generos = com.split("\\|");

		for(String genero : generos){
			gen.agregarElementoFinal(genero);
		}

		film.setGenerosAsociados(gen);
		int first = line.indexOf(",");
		String nom;

		if (line.contains("\"")){
			nom = (String) line.subSequence(first+1, last);
		}
		else {
			nom = (String) line.subSequence(first+1, last-6);
		}
		film.setTitulo(nom);
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		String sinGeneros = (String) line.subSequence(0, last);
		int p = sinGeneros.lastIndexOf("(");
		if(p != -1){
			String anio = sinGeneros.replaceAll("\""," " ).trim().substring(p+1,p+5);

			film.setAgnoPublicacion(Integer.parseInt(anio));
		}

		else{
			film.setAgnoPublicacion(0000);
		}

		System.out.println("Nombre:"+film.getTitulo() +" a�o: " + film.getAgnoPublicacion() +" generos: "+ Arrays.toString(generos));
		return film;

	}

	private VORating nuevoRating(String line){
		String cadena[] = line.split(",");
		VORating rating = new VORating();

		double b = Double.parseDouble(cadena[2]);
		rating.setRating(b);

		long n = Long.parseLong(cadena[0]);
		rating.setIdUsuario(n);

		long n1 = Long.parseLong(cadena[1]);
		rating.setIdPelicula(n1);

		long n2 = Long.parseLong(cadena[3]);
		rating.setTimestamp(n2);

		double promedioAnterior = (misPeliculas.darElemento((int) n1).getPromedioRatings()) * misPeliculas.darElemento((int) n1).getNumeroRatings();
		misPeliculas.darElemento((int) n1).setNumeroRatings(misPeliculas.darElemento((int) n1).getNumeroRatings() +1);		
		misPeliculas.darElemento((int) n1).setPromedioRatings((promedioAnterior + b)/misPeliculas.darElemento((int) n1).getNumeroRatings()); 
		rating.setIdUsuario(n1);

		rating.setTimestamp(n2);
		System.out.println(rating.getIdPelicula() + "   "+  rating.getIdUsuario() + "   "+ rating.getRating());
		return rating;
	}

	private VOTag nuevoTag(String line){
		String cadena[] = line.split(",");
		VOTag tag = new VOTag();

		long n = Long.parseLong(cadena[0]);
		tag.setIdUsuario(n);

		long n1 = Long.parseLong(cadena[1]);
		tag.setIdPelicula(n1);

		tag.setIdUsuario(n1);

		tag.setTag(cadena[2]);

		long n2 = Long.parseLong(cadena[3]);
		tag.setTimestamp(n2);


		ILista<VOTag> tags = misPeliculas.darElemento((int) n1).getTagsAsociados();
		tags.agregarElementoFinal(tag);
		misPeliculas.darElemento((int) n1).setTagsAsociados(tags);
		misPeliculas.darElemento((int) n1).setNumeroTags(misPeliculas.darElemento((int) n1).getNumeroTags() +1);
		System.out.println(tag.getIdPelicula() + "   " + tag.getIdUsuario() + "   " + tag.getTag());

		return tag;

	}

	public ILista<VOGenero> nuevosGeneros(String line){
		ILista<VOGenero> generos = new ListaDobleEncadenada<VOGenero>();
		String cadena[] = line.split(",");
		for(int i = 0; i < (cadena[2].split("|")).length; i++){
			VOGenero genero = new VOGenero();
			genero.setGenero(cadena[2].split("|")[i]);
			generos.agregarElementoFinal(genero);
		}
		return generos;
	}

	public void agregarGeneros(ILista<VOGenero> gens ){
		if(generos.isEmpty()){
			for(int i = 0 ; i < gens.darNumeroElementos(); i ++){
				VOGenero gen = new VOGenero();
				generos.agregarElementoFinal(gen);
			}
		}
		else{
			for(int i = 0; i < generos.darNumeroElementos(); i ++){
				if(!existeGenero(generos.darElemento(i))){
					VOGenero gen = new VOGenero();
					generos.agregarElementoFinal(gen);
				}
			}
		}
	}

	public boolean existeGenero(VOGenero gen){
		boolean existe = false;
		for (int i = 0; i < generos.darNumeroElementos(); i++){
			if(generos.darElemento(i) == gen){
				existe = true;
				break;
			}
		}
		return existe;
	}


	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		misPeliculas = new ListaEncadenada<VOPelicula>();
		boolean hay = false;
		BufferedReader br;

		try {

			br=new BufferedReader(new FileReader(rutaPeliculas));
			String line = br.readLine();
			line = br.readLine();
			while(line != null ){
				misPeliculas.agregarElementoFinal(nuevaPelicula(line));
				agregarGeneros(nuevosGeneros(line));
				line = br.readLine();
			}
			System.out.println("SE CARGARON LAS PELICULAS");
			hay = true;
			br.close();
		} 

		catch (Exception e) {			
			e.printStackTrace();
		}
		return hay;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		misRatings = new ListaEncadenada<VORating>();
		boolean hay = false;
		BufferedReader br;

		try {

			br=new BufferedReader(new FileReader(rutaRatings));
			String line = br.readLine();
			line = br.readLine();
			while(line != null ){
				misRatings.agregarElementoFinal(nuevoRating(line));
				line = br.readLine();
			}
			System.out.println("SE CARGARON LOS RATINGS");
			hay = true;
			br.close();
		} 

		catch (Exception e) {			
			e.printStackTrace();
		}
		return hay;
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		misTags = new ListaEncadenada<VOTag>();
		boolean hay = false;
		BufferedReader br;

		try {

			br=new BufferedReader(new FileReader(rutaTags));
			String line = br.readLine();
			line = br.readLine();
			while(line != null ){
				misTags.agregarElementoFinal(nuevoTag(line));
				line = br.readLine();
			}
			System.out.println("SE CARGARON LOS TAGS");
			hay = true;
			br.close();
		} 

		catch (Exception e) {			
			e.printStackTrace();
		}
		return hay;
	}

	public boolean cargarUsuariosSR(String ruta){
		usuarios = new ListaEncadenada<VOUsuario>();
		boolean hay = false;
		BufferedReader br;
		VOUsuario user = null;
		ILista<Integer> temp = new ListaEncadenada<Integer>();
		int numRatings=0;
		try {

			br=new BufferedReader(new FileReader(ruta));
			String line = br.readLine();
			line = br.readLine();
			while(line != null ){
				numRatings = 0;
				user = nuevoUsuario(line);
				usuarios.agregarElementoFinal(user);
				while(br.readLine().startsWith(user.getIdUsuario() + "")){
					String[] let = line.split(",");
					numRatings ++;
					temp.agregarElementoFinal(Integer.parseInt(let[3]));
				}
				user.setNumRatings(numRatings);
				user.setPrimerTimestamp(darMenorTime(temp));
			}
			System.out.println("SE CARGARON LOS USUARIOS");
			hay = true;
			br.close();
		} 

		catch (Exception e) {			
			e.printStackTrace();
		}
		return hay;
	}

	public long darMenorTime(ILista<Integer> lista){
		long mejor = 0;
		for(int i = 0; i < lista.darNumeroElementos(); i++){
			if(lista.darElemento(i) > mejor){
				mejor = (long) lista.darElemento(i);
			}
		}
		return mejor;
	}


	public VOUsuario nuevoUsuario(String line){
		String cadena[] = line.split(",");
		VOUsuario user = new VOUsuario();

		int b = Integer.parseInt(cadena[0]);
		user.setIdUsuario(b);
		System.out.println(user.getIdUsuario() + "   " );
		return user;
	}


	@Override
	public int sizeMoviesSR() {
		return misPeliculas.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() {
		return usuarios.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() {
		return misTags.darNumeroElementos();
	}

	public int sizeRatingsSR(){
		return misRatings.darNumeroElementos();
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		listasPeliGeneros(n);
		ordenarPeliculasSegunRating(listaPeliculaGenero);
		return listaPeliculaGenero;
	}

	private void ordenarPeliculasSegunRating(ILista<VOGeneroPelicula> lista){
		for(int i = 0; i < lista.darNumeroElementos(); i++){
			MergeSortPorRating.sort(lista.darElemento(i).getPeliculas());
		}
		

	}


	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		MergeSortGeneral<VOPelicula> merge = new MergeSortGeneral<VOPelicula>();
		merge.sort(misPeliculas);
		return misPeliculas;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		listasPeliGeneros();
		ordenarPeliculasSegunRating(listaPeliculaGenero);
		return listaPeliculaGenero;
	}

	private void listasPeliGeneros()
	{
		ILista<VOGeneroPelicula> lista = new ListaEncadenada<>();
		boolean hay = false;
		for(int i = 0; i < generos.darNumeroElementos(); i++){
			VOGeneroPelicula genPel = new VOGeneroPelicula();
			genPel.setGenero(generos.darElemento(i).getGenero());
			lista.agregarElementoFinal(genPel);
		}

		for(int i = 0; i < misPeliculas.darNumeroElementos(); i++){
			for(int j = 0 ; j < misPeliculas.darElemento(i).getGenerosAsociados().darNumeroElementos(); i++){
				VOGenero var = new VOGenero();
				var.setGenero(misPeliculas.darElemento(i).getGenerosAsociados().darElemento(j));
				if(existeGeneroTagsGenero(var, lista)){
					hay = true;
					for(int k = 0; k < lista.darNumeroElementos() && hay; k++){
						if(lista.darElemento(k).getGenero() == var.getGenero()){
							lista.darElemento(k).getPeliculas().agregarElementoFinal(misPeliculas.darElemento(i));
							hay = false;

						}
					}
				}
				else{
					VOGeneroPelicula nuevo = new VOGeneroPelicula();
					nuevo.setGenero(var.getGenero());
					nuevo.getPeliculas().agregarElementoFinal(misPeliculas.darElemento(i));
					lista.agregarElementoFinal(nuevo);
				}
			}
		}
		listaPeliculaGenero = lista;
	}	

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		// TODO Auto-generated method stub
		listasPeliGeneros();
		ordenarPeliculasSegunAgno(listaPeliculaGenero);
		return listaPeliculaGenero;
	}
	private void ordenarPeliculasSegunAgno(ILista<VOGeneroPelicula> lista){
		for(int i = 0; i < lista.darNumeroElementos(); i++){
			MergeSortPorAgno.sort(lista.darElemento(i).getPeliculas());
		}
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		
		ILista<VOPeliculaPelicula> lista = new ListaEncadenada<VOPeliculaPelicula>();
		cargarRecomendaciones(rutaRecomendacion);
		VOPelicula varPel = null;
		double varRatPel = 0;
		for(int i = 0; i < colaPelicula.size(); i++){
			varPel = colaPelicula.dequeue();

			VOPeliculaPelicula nuevo = new VOPeliculaPelicula();
			nuevo.setPelicula(varPel);
			nuevo.setPeliculasRelacionadas(peliculasDelGenero(varPel.getGenerosAsociados().darElemento(0)));
			lista.agregarElementoFinal(nuevo);
			for(int j = 0; j < misPeliculas.darNumeroElementos(); j++){
					if(usuarioQueSirve(varRatPel, misPeliculas.darElemento(j).getPromedioRatings())){
						lista.agregarElementoFinal(nuevo);
						}
					}

				}

		return lista;
	}
	private ILista<VOPelicula> peliculasDelGenero(String s)
	{
		ILista<VOPelicula> lista = null;
		int i=0;
		while(!listaPeliculaGenero.isEmpty())
		{
			i++;
			if(s.equals(listaPeliculaGenero.darElemento(i).getGenero()))
				lista = listaPeliculaGenero.darElemento(i).getPeliculas();
		}
		return lista;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		MergeSortGeneral<VORating> merge = new MergeSortGeneral<VORating>();
		merge.sort(misRatings); 
		return misRatings;
	}

	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		ILista<VOGeneroUsuario> lista = new ListaDobleEncadenada<>();
		for(int i = 0; i < generos.darNumeroElementos(); i++){
			VOGeneroUsuario genPel = new VOGeneroUsuario();
			genPel.setGenero(generos.darElemento(i).getGenero());
			lista.agregarElementoFinal(genPel);
		}
		boolean hay = false;
		long idPel;
		for(int i = 0; i < misRatings.darNumeroElementos(); i ++){
			idPel = misRatings.darElemento(i).getIdPelicula();
			for(int j = 0; j < misPeliculas.darNumeroElementos(); j++){
				if(misPeliculas.darElemento(j).getIdPelicula() == idPel){
					ILista<String> var = misPeliculas.darElemento(i).getGenerosAsociados();
					for (int k = 0; k < var.darNumeroElementos(); k++){
						if(existeGeneroRatingUsuario(var.darElemento(k), lista)){
							for(int l = 0; l < lista.darNumeroElementos() && !hay; l++){
								if(lista.darElemento(l).getGenero() == var.darElemento(k)){
									if(lista.darElemento(k).getUsuarios().darNumeroElementos() <= n){
										lista.darElemento(k).getUsuarios().agregarElementoFinal(darUsuarioConteoId(misRatings.darElemento(i).getIdUsuario()));
										hay = false;
									}
								}
							}
						}
						else{
							VOGeneroUsuario nuevo = new VOGeneroUsuario();
							nuevo.setGenero(var.darElemento(i));
							nuevo.setUsuarios(new ListaEncadenada<VOUsuarioConteo>());
							nuevo.getUsuarios().agregarElementoFinal(darUsuarioConteoId(misRatings.darElemento(i).getIdUsuario()));			
							lista.agregarElementoFinal(nuevo);
						}
					}
				}
			}
		}
		ordenarUsuariosConteo(lista);
		return lista;
	}

	public void ordenarUsuariosConteo(ILista<VOGeneroUsuario> lis){
		MergeSortGeneral<VOUsuarioConteo> merge = new MergeSortGeneral<VOUsuarioConteo>();
		for(int i = 0; i < lis.darNumeroElementos(); i++){
			merge.sort(lis.darElemento(i).getUsuarios()); 
		}
	}

	public VOUsuarioConteo darUsuarioConteoId(long id){
		VOUsuarioConteo us = null;
		for(int i = 0; i < usuarios.darNumeroElementos(); i++){
			if(usuarios.darElemento(i).getIdUsuario() == id){
				us = new VOUsuarioConteo();
				us.setIdUsuario(id);
				us.setConteo(usuarios.darElemento(i).getNumRatings());
			}
		}
		return us;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		MergeSortGeneral<VOUsuario> merge = new MergeSortGeneral<VOUsuario>();
		merge.sort(usuarios);
		return usuarios;
	}

	public boolean existeGeneroRatingUsuario(String gen, ILista<VOGeneroUsuario> lis){
		boolean existe = false;
		String var = null;
		for(int i = 0; i < lis.darNumeroElementos(); i++){
			var = lis.darElemento(i).getGenero();
			if(var == gen){
				existe = true;
				break;
			}
		}
		return existe;

	}


	public boolean existeGeneroTagsGenero(VOGenero gen, ILista<VOGeneroPelicula> lis){
		boolean existe = false;
		String var = null;
		for(int i = 0; i < lis.darNumeroElementos(); i++){
			var = lis.darElemento(i).getGenero();
			if(var == gen.getGenero()){
				existe = true;
				break;
			}
		}
		return existe;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		listasPeliGeneros(n);
		ordenarPeliculasSegunTags(listaPeliculaGenero);		
		return listaPeliculaGenero;
	}

	private void listasPeliGeneros(int n)
	{
		ILista<VOGeneroPelicula> lista = new ListaEncadenada<>();
		boolean hay = false;
		for(int i = 0; i < generos.darNumeroElementos(); i++){
			VOGeneroPelicula genPel = new VOGeneroPelicula();
			genPel.setGenero(generos.darElemento(i).getGenero());
			lista.agregarElementoFinal(genPel);
		}

		for(int i = 0; i < misPeliculas.darNumeroElementos(); i++){
			for(int j = 0 ; j < misPeliculas.darElemento(i).getGenerosAsociados().darNumeroElementos(); i++){
				VOGenero var = new VOGenero();
				var.setGenero(misPeliculas.darElemento(i).getGenerosAsociados().darElemento(j));
				if(existeGeneroTagsGenero(var, lista)){
					hay = true;
					for(int k = 0; k < lista.darNumeroElementos() && hay; k++){
						if(lista.darElemento(k).getGenero() == var.getGenero()){
							if(lista.darElemento(k).getPeliculas().darNumeroElementos() <= n){
								lista.darElemento(k).getPeliculas().agregarElementoFinal(misPeliculas.darElemento(i));
								hay = false;
							}
						}
					}
				}
				else{
					VOGeneroPelicula nuevo = new VOGeneroPelicula();
					nuevo.setGenero(var.getGenero());
					nuevo.getPeliculas().agregarElementoFinal(misPeliculas.darElemento(i));
					lista.agregarElementoFinal(nuevo);
				}
			}
		}
		listaPeliculaGenero = lista;
	}	

	public void ordenarPeliculasSegunTags(ILista<VOGeneroPelicula> lista){

		for(int i = 0; i < lista.darNumeroElementos(); i++){
			MergeSortPorNumeroTags.sort(lista.darElemento(i).getPeliculas());
		}

	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		ILista<VOUsuarioGenero> lista = new ListaEncadenada<VOUsuarioGenero>();
		for(int i = 0; i < usuarios.darNumeroElementos(); i++){
			VOUsuarioGenero nuevo = new VOUsuarioGenero();
			nuevo.setIdUsuario(usuarios.darElemento(i).getIdUsuario());
			nuevo.setListaGeneroTags(new ListaEncadenada<VOGeneroTag>());
			lista.agregarElementoFinal(nuevo);
		}


		for(int i = 0; i < misTags.darNumeroElementos(); i++){
			for(int j = 0 ; j < lista.darNumeroElementos(); j++){
				if(lista.darElemento(j).getIdUsuario() == misTags.darElemento(i).getIdUsuario()){
					ILista<String> genAso = (buscarPeliculaId(misTags.darElemento(i).getIdPelicula())).getGenerosAsociados();
					for(int k = 0; k < genAso.darNumeroElementos(); k++){
						if(existeListaGenero(genAso.darElemento(k), lista.darElemento(j).getListaGeneroTags())){
							for(int l = 0; l < lista.darElemento(j).getListaGeneroTags().darNumeroElementos(); l++){
								if(lista.darElemento(j).getListaGeneroTags().darElemento(l).getGenero() == genAso.darElemento(k)){
									lista.darElemento(j).getListaGeneroTags().darElemento(l).getTags().agregarElementoFinal(misTags.darElemento(i));
								}
							}
						}
						else{
							VOGeneroTag nuevo = new VOGeneroTag();
							nuevo.setGenero(genAso.darElemento(k));
							nuevo.setTags(new ListaEncadenada<VOTag>());
							nuevo.getTags().agregarElementoFinal(misTags.darElemento(i));
							lista.darElemento(j).getListaGeneroTags().agregarElementoFinal(nuevo);
						}
					}
				}
			}
		}
		ordenarTagsAlfabetico(lista);
		return lista;
	}

	public void ordenarTagsAlfabetico(ILista<VOUsuarioGenero> lis){
		for(int i = 0; i < lis.darNumeroElementos(); i++){
			for(int j =0; j < lis.darElemento(i).getListaGeneroTags().darNumeroElementos();j++)
				MergeSortTagsAlfabetico.sort(lis.darElemento(i).getListaGeneroTags().darElemento(j).getTags());
		}
	}

	private VOPelicula buscarPeliculaId(long id){
		VOPelicula peli = null;
		for(int i = 0; i < misPeliculas.darNumeroElementos(); i++){
			if(misPeliculas.darElemento(i).getIdPelicula() == id){
				peli =misPeliculas.darElemento(i);
			}
		}
		return peli;
	}

	public boolean existeListaGenero(String gen, ILista<VOGeneroTag>lis){
		boolean existe = false;
		for(int i = 0; i < lis.darNumeroElementos(); i++){
			if(lis.darElemento(i).getGenero() == gen){
				existe = true;
			}
		}
		return existe;
	}
	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		ILista<VOPeliculaUsuario> lista = new ListaEncadenada<VOPeliculaUsuario>();
		cargarRecomendaciones(rutaRecomendacion);
		VOPeliculaRating varPel = null;
		long varIdPel = 0;
		double varRatPel = 0;
		for(int i = 0; i < cola.size(); i++){
			varPel = cola.dequeue();
			varIdPel = varPel.getIdPelicula();
			varRatPel = varPel.getRating();
			VOPeliculaUsuario nuevo = new VOPeliculaUsuario();
			nuevo.setIdPelicula(varIdPel);
			nuevo.setUsuariosRecomendados(new ListaEncadenada<VOUsuario>());
			lista.agregarElementoFinal(nuevo);
			for(int j = 0; j < misRatings.darNumeroElementos(); j++){
				if(misRatings.darElemento(j).getIdPelicula() == varIdPel){
					if(usuarioQueSirve(varRatPel, misRatings.darElemento(j).getRating())){
						for(int k = 0;  k < lista.darNumeroElementos(); k++){
							if(lista.darElemento(k) == nuevo){
								lista.darElemento(k).getUsuariosRecomendados().agregarElementoFinal(buscarUsuarioId(misRatings.darElemento(j).getIdUsuario()));
							}
						}
					}

				}
			}
		}
		ordenarUsuariosRecomendados(lista);
		return lista;
	}

	public void ordenarUsuariosRecomendados(ILista<VOPeliculaUsuario> lis){
		for(int i = 0 ; i < lis.darNumeroElementos(); i++){
			ILista<VOUsuarioRecomendado> recomendados = new ListaEncadenada<VOUsuarioRecomendado>();
			for(int j = 0; j < lis.darElemento(i).getUsuariosRecomendados().darNumeroElementos(); j++){
				VOUsuarioRecomendado nuevoTemp = new VOUsuarioRecomendado();
				nuevoTemp.setIdPelicula(lis.darElemento(i).getIdPelicula());
				nuevoTemp.setIdUsuario(lis.darElemento(i).getUsuariosRecomendados().darElemento(j).getIdUsuario());
				recomendados.agregarElementoFinal(nuevoTemp);
			}
			ordenarRecomendados(recomendados);
			
			ILista<VOUsuario> nuevosRecomendados = new ListaEncadenada<VOUsuario>();
			for(int k = 0; k < recomendados.darNumeroElementos(); k++){
				VOUsuario nuevoRecomendado = new VOUsuario();
				nuevoRecomendado.setIdUsuario(recomendados.darElemento(k).getIdUsuario());
				nuevosRecomendados.agregarElementoFinal(nuevoRecomendado);
			}
			lis.darElemento(i).setUsuariosRecomendados(nuevosRecomendados);
		}
	}

	public void ordenarRecomendados(ILista<VOUsuarioRecomendado> lis){
		MergeSortGeneral<VOUsuarioRecomendado> merge = new MergeSortGeneral<VOUsuarioRecomendado>();
		merge.sort(lis);
	}
	
	public VOUsuario buscarUsuarioId(long id){
		VOUsuario esteEs = null;
		for(int i = 0; i < usuarios.darNumeroElementos(); i++){
			if(usuarios.darElemento(i).getIdUsuario() == id){
				esteEs = usuarios.darElemento(i);
				break;
			}
		}
		return esteEs;
	}

	public boolean usuarioQueSirve(double califPrin, double califPosible){
		boolean sirve = false;
		if( ( (califPrin - califPosible) <= 0.5) || (  ( (califPrin-califPosible)*(-1)) <= 0.5)  ){
			sirve = true;
		}
		return sirve;
	}

	public boolean cargarRecomendaciones(String ruta){
		boolean hay = false;
		BufferedReader br;
		try {
			br=new BufferedReader(new FileReader(ruta));
			String line = br.readLine();
			line = br.readLine();
			while(line != null ){
				cola.enqueue(nuevaMovie(line));
				line = br.readLine();
			}
			System.out.println("SE CARGARON LAS RECOMENDACIONES A LA COLA");
			hay = true;
			br.close();
		} 

		catch (Exception e) {			
			e.printStackTrace();
		}
		return hay;	
	}

	public VOPeliculaRating nuevaMovie(String line){
		VOPeliculaRating peli = new VOPeliculaRating();
		String cadena[] = line.split(",");

		peli.setIdPelicula(Long.parseLong(cadena[0]));
		peli.setRating(Double.parseDouble(cadena[1]));
		return peli;
	}
	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		ILista<VOTag> ret = null;
		for(int i = 0; i < misPeliculas.darNumeroElementos(); i++){
			if(misPeliculas.darElemento(i).getIdPelicula() == idPelicula){
				ret = misPeliculas.darElemento(i).getTagsAsociados();
			}
		}
		ordenarTags(ret);
		return ret;
	}

	public ILista<VOTag> ordenarTags(ILista<VOTag> lis){
		MergeSortGeneral<VOTag> merge = new MergeSortGeneral<VOTag>();
		merge.sort(lis);
		return lis;
	}

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		VOOperacion operacion = new VOOperacion();
		operacion.setOperacion(nomOperacion);
		operacion.setTimestampInicio(tinicio);
		operacion.setTimestampFin(tfin);
		misOperaciones.push(operacion);		

	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		model.data_structures.Queue<VOOperacion> filaoperaciones = new model.data_structures.Queue<VOOperacion>();
		ILista<VOOperacion> listaOperaciones = new ListaEncadenada<>();
		while(!misOperaciones.isEmpty())
		{
			VOOperacion operacion = misOperaciones.pop();
			listaOperaciones.agregarElementoFinal(operacion);
			filaoperaciones.enqueue(operacion);
		}
		while(!filaoperaciones.isEmpty())
		{
			misOperaciones.push(filaoperaciones.dequeue());
		}

		return listaOperaciones;
	}

	@Override
	public void limpiarHistorialOperacionesSR() {
		while(!misOperaciones.isEmpty())
		{
			misOperaciones.pop();
		}

	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		model.data_structures.Queue<VOOperacion> filaoperaciones = new model.data_structures.Queue<VOOperacion>();
		ILista<VOOperacion> listaOperaciones = new ListaEncadenada<>();
		for(int i = 0;i<n; i++)
		{
			VOOperacion operacion = misOperaciones.pop();
			listaOperaciones.agregarElementoFinal(operacion);
			filaoperaciones.enqueue(operacion);
		}
		while(!filaoperaciones.isEmpty())
		{
			misOperaciones.push(filaoperaciones.dequeue());
		}
		return listaOperaciones;
	}

	@Override
	public void borrarUltimasOperaciones(int n) {
		for(int i = 0;i<n; i++)
		{
			misOperaciones.pop();
		}

	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {
		VOPelicula pelicula = new VOPelicula();
		pelicula.setTitulo(titulo);
		pelicula.setAgnoPublicacion(agno);
		ILista<String> gen = new ListaEncadenada<String>();
		for(int i=0; i<generos.length;i++)
		{
			gen.agregarElementoFinal(generos[i]);
		}
		pelicula.setGenerosAsociados(gen);
		pelicula.setNumeroRatings(0);
		pelicula.setNumeroTags(0);
		pelicula.setPromedioRatings(0);
		pelicula.setIdPelicula(misPeliculas.darNumeroElementos() +1);
		misPeliculas.agregarElementoFinal(pelicula);
		return pelicula.getIdPelicula();
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		VORating ratin = new VORating();
		ratin.setIdPelicula(idPelicula);
		ratin.setIdUsuario(idUsuario);
		ratin.setTimestamp(System.currentTimeMillis());
		ratin.setRating(rating);

		usuarios.darElemento(idUsuario).setNumRatings(usuarios.darElemento(idUsuario).getNumRatings()+1);;
		double promedioAnterior = (misPeliculas.darElemento(idPelicula).getPromedioRatings()) * misPeliculas.darElemento(idPelicula).getNumeroRatings();
		misPeliculas.darElemento(idPelicula).setNumeroRatings(misPeliculas.darElemento(idPelicula).getNumeroRatings() +1);		
		misPeliculas.darElemento(idPelicula).setPromedioRatings((promedioAnterior + rating)/misPeliculas.darElemento(idPelicula).getNumeroRatings()); 	}
}
