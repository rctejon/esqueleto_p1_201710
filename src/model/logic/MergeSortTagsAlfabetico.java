package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.vo.VOTag;

public class MergeSortTagsAlfabetico {
	// Codigo sacado del libro gu�a del curso.
	
		private  static ILista<VOTag> aux;

		public static void sort(ILista<VOTag> arr)
		{
			
			aux = new ListaDobleEncadenada<VOTag>(); 
			sort(arr, 0, arr.darNumeroElementos() - 1);
		}


		private  static void sort(ILista<VOTag> arr, int men, int may){
			if (may <= men){
				return;
			}

			int mid = men + (may - men)/2;
			sort(arr, men, mid); 
			sort(arr, mid+1, may); 
			merge(arr, men, mid, may); 
		}

		public static void merge(ILista<VOTag> arr, int men, int mitad, int may)
		{ 
			int i = men, j = mitad+1;
			for (int k = men; k <= may; k++){
				aux.setElemento(k, arr.darElemento(k));
			}

			
			for (int k = men; k <= may; k++){ 
				if (i > mitad) {
					arr.setElemento(k, aux.darElemento(j++));
				}
				else if (j > may ){
					arr.setElemento(k, aux.darElemento(i++));
				}
				
				else if( less(aux.darElemento(j), aux.darElemento(i)) ) {

					arr.setElemento(k, aux.darElemento(j++));
				}
				else{
					arr.setElemento(k, aux.darElemento(i++));
				}
			}
		}
		
		public  static boolean less(VOTag v , VOTag w){
			return v.compareTo1(w) < 0;
		}
}
