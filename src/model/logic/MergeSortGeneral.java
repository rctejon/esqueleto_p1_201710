package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOUsuario;



public class MergeSortGeneral<T extends Comparable<T>> {

	// Codigo sacado del libro gu�a del curso.
	
	private  ILista<T> aux;

	public void sort(ILista<T> arr)
	{
		
		aux = new ListaDobleEncadenada<T>(); 
		sort(arr, 0, arr.darNumeroElementos() - 1);
	}


	private  void sort(ILista<T> arr, int men, int may){
		if (may <= men){
			return;
		}

		int mid = men + (may - men)/2;
		sort(arr, men, mid); 
		sort(arr, mid+1, may); 
		merge(arr, men, mid, may); 
	}

	public  void merge(ILista<T> arr, int men, int mitad, int may)
	{ 
		int i = men, j = mitad+1;
		for (int k = men; k <= may; k++){
			aux.setElemento(k, arr.darElemento(k));
		}

		
		for (int k = men; k <= may; k++){ 
			if (i > mitad) {
				arr.setElemento(k, aux.darElemento(j++));
			}
			else if (j > may ){
				arr.setElemento(k, aux.darElemento(i++));
			}
			
			else if( less(aux.darElemento(j), aux.darElemento(i)) ) {

				arr.setElemento(k, aux.darElemento(j++));
			}
			else{
				arr.setElemento(k, aux.darElemento(i++));
			}
		}
	}
	
	public  boolean less(T v , T w){
		return v.compareTo(w) < 0;
	}
}
