package model.vo;

public class VOUsuarioRecomendado implements Comparable<VOUsuarioRecomendado>{

	private long idUsuario;
	private double rating;
	private long idPelicula;

	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}

	@Override
	public int compareTo(VOUsuarioRecomendado o) {
		int res = 0;
		if(this.getRating() < o.getRating()){
			res = -1;
		}
		else if(this.getRating() >= o.getRating()){
			res = 1;
		}
		return res;
	}


}
