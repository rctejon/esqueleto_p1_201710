package model.vo;

import model.data_structures.ILista;

public class VOGeneroTag implements Comparable<VOGeneroTag>{

	private ILista<VOTag> tags;
	private String genero;
	public ILista<VOTag> getTags() {
		return tags;
	}
	public void setTags(ILista<VOTag> tags) {
		this.tags = tags;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int compareTo(VOGeneroTag o) {
		return 0;
	}
	
	
}
