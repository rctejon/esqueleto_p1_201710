package model.vo;

import model.data_structures.ILista;

public class VOUsuarioGenero implements Comparable<VOUsuarioGenero>{

	private long idUsuario;

	private ILista<VOGeneroTag> listaGeneroTags;

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public ILista<VOGeneroTag> getListaGeneroTags() {
		return listaGeneroTags;
	}

	public void setListaGeneroTags(ILista<VOGeneroTag> listaGeneroTags) {
		this.listaGeneroTags = listaGeneroTags;
	}

	@Override
	public int compareTo(VOUsuarioGenero o) {

		return 0;
	}





}
