package model.vo;

public class VOOperacion implements Comparable<VOOperacion> {
	
	private String operacion;
	private long timestampInicio;
	private long timestampFin;
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public long getTimestampInicio() {
		return timestampInicio;
	}
	public void setTimestampInicio(long timestampInicio) {
		this.timestampInicio = timestampInicio;
	}
	public long getTimestampFin() {
		return timestampFin;
	}
	public void setTimestampFin(long timestampFin) {
		this.timestampFin = timestampFin;
	}

	@Override
	public int compareTo(VOOperacion o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
