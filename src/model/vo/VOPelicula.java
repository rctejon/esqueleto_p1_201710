package model.vo;

import model.data_structures.ILista;


public class VOPelicula implements Comparable<VOPelicula> {

	private long idPelicula; 
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;

	private ILista<VOTag> tagsAsociados;
	private ILista<String> generosAsociados;



	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<VOTag> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<VOTag> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	
	public int compareTo1(VOPelicula o) {
		if(this.getNumeroTags() < o.getNumeroTags()){
			return -1;
		}
		else{
			return 1;
		}
	}
	

	@Override
	public int compareTo(VOPelicula o) {
		int a�o = agnoPublicacion - o.getAgnoPublicacion();
		if(a�o == 0)
		{	
			int r = (int) (promedioRatings - o.promedioRatings);
			if(r == 0)
			{
				return titulo.compareTo(o.getTitulo());
			}
			return r;
		}	
		return a�o;
	}

}
