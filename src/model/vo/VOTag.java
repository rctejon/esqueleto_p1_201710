package model.vo;

public class VOTag implements Comparable<VOTag>{
	private long idUsuario;
	private long idPelicula;
	private String tag;
	private long timestamp;
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdUsuario(){
		return this.idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	
	@Override
	public int compareTo(VOTag o) {
		if(this.timestamp < o.timestamp){
			return -1;
		}
		else{
			return 1;
		}
	}
	
	public int compareTo1(VOTag o){
		if(this.getTag().compareToIgnoreCase(o.getTag()) < 0){
			return -1;
		}
		else{
			return 1;
		}
	}
	
}
