package model.vo;

public class VOPeliculaRating implements Comparable<VOPeliculaRating>{

	private long idPelicula;
	private double rating;
	
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	@Override
	public int compareTo(VOPeliculaRating o) {
		return 0;
	}
	
	
}
