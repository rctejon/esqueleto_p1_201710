package model.vo;

public class VORating implements Comparable<VORating> {
	
	private long idUsuario;
	private long idPelicula;
	private double rating;
	private long timestamp;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public int compareTo(VORating o) {
		// TODO Auto-generated method stub

		return (int) (timestamp - o.getTimestamp());
	}
	

}
