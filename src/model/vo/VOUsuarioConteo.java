package model.vo;

public class VOUsuarioConteo implements Comparable<VOUsuarioConteo>{
	private long idUsuario;
	private int conteo;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	@Override
	public int compareTo(VOUsuarioConteo o) {
		if(this.getConteo() < o.getConteo()){
			return -1;
		}
		else{
			return 1;
		}
	}
	
}
