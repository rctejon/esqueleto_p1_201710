package model.data_structures;

import java.util.Iterator;


public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> first;
	private NodoDoble<T> actual;
	private NodoDoble<T> last;
	private int pos;
	private int size;

	public ListaDobleEncadenada()
	{
		first = null;
		actual = first;
		last = null;
		pos = 0;
		size = 0;
	}
	@Override
	public Iterator<T> iterator() {

		return new Iterator<T>(){
			NodoDoble<T> act = null;


			public boolean hasNext()
			{
				if(size==0)
					return false;
				if(act==null)
					return true;

				return act.getNext()!= null;
			}


			public T next(){
				if (hasNext())
					return act.getNext().getItem();
				else
					return first.getItem();	

			}

			@Override
			public void remove() {
				act.remove();
			}
		};
	}  

	@Override
	public void agregarElementoFinal(T elem) 
	{
		NodoDoble<T> newElement = new NodoDoble<T>();
		newElement.setItem(elem);
		newElement.setNext(null);
		if(first==null){
			size++;
			first = newElement;
			last = newElement;
		}
		else{
			newElement.setPrev(last);
			last.setNext(newElement);
			last = newElement;
		}
		size++;
		actual = first;
	}

	@Override
	public T darElemento(int pPos) {
		actual=first;
		if(actual!=null)
		{	
			for (int i = 0; i < pPos; i++) {
				actual = actual.getNext();
			}
			pos = pPos;
			return actual.getItem();
		}

		return null;
	}


	@Override
	public int darNumeroElementos()
	{
		// TODO Auto-generated method stub
		return size;
	}

	public T darElementoPosicionActual()
	{
		// TODO Auto-generated method stub
		if(actual!=null)
			return actual.getItem();
		else 
			return null;
	}

	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub

		if(actual!=null && actual.hasNext()	){
			actual = actual.getNext();
			pos++;
			return true;
		}
		return false;

	}

	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(actual!=null&&actual.getPrev()!= null){
			actual = actual.getPrev();
			pos--;
			return true;
		}
		return false;
	}
	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean isEmpty() {
		return first == null;
	}

	public void setElemento(int pos, T item){
		NodoDoble<T> var = new NodoDoble<>(item);
		if(pos <= size){
			actual = first;
			for(int i = 0; i < pos; i++){
				avanzarSiguientePosicion();
			}
			actual.getNext().setPrev(var);
			actual.setNext(var);
		}
		else{
			agregarElementoFinal(item);
		}	
	}

}
