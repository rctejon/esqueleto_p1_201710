package model.data_structures;


public class NodoDoble<T> 
{
	
	private T item;
	
	private NodoDoble<T> next;
	
	private NodoDoble<T> prev;
	


	
	public NodoDoble(T item){
		next = null;		
		prev = null;
		this.item = item;
	}
	
	public NodoDoble() 
	{
		next = null;		
		prev = null;
	}
	
	
	public void setNext(NodoDoble<T> pNodo)
	{
		next = pNodo;
	}
	public void setPrev(NodoDoble<T> pNodo)
	{
		prev = pNodo;
	}
	
	public boolean hasNext() {
		if(next != null ){
			return true;
		}
		return false;
	}

	public NodoDoble<T> getNext() {		
		return   next;
	}
	
	public NodoDoble<T> getPrev() {		
		return   prev;
	}
	
	public T getItem()
	{
		return item;
	}
	public void setItem(T pItem)
	{
		item = pItem;
	}
	
	public void remove() {
		 prev.setNext(next);
		 next.setPrev(prev);
	}
	
	 
	
	
}
