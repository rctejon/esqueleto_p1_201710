package model.data_structures;

public class Stack<T extends Comparable<T>> 
{
	ListaEncadenada<T> list;
	int tope;
	int anterior;

	public Stack ()
	{
		list = new ListaEncadenada<T>();
		tope = 0;
		anterior=-1;
	}

	public void push(T item)
	{

		list.agregarElementoFinal(item);
		tope++;
		anterior++;
	}

	public T pop()
	{
		
		T t= list.darElemento(tope);
		
		if(tope >0)
		{	
			list.remove(tope);
			tope--;
			anterior--;
			return t;

		}
		else
		{
			return null;
		}

	}

	public boolean isEmpty()
	{
		return tope==0;
	}
	public int size()
	{
		return tope;
	}

}
