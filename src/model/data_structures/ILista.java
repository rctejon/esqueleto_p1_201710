package model.data_structures;

public interface ILista<T> extends Iterable<T>{
	
	
	
	public void agregarElementoFinal(T elem);
	
	public T darElemento(int pos);
	
	public T eliminarElemento(int pos);
	
	public int darNumeroElementos();
	
	public boolean isEmpty();
	
	public void setElemento(int pos, T item);
	
	

}
