package model.data_structures;

public class Queue <T extends Comparable<T>>
{
	ListaEncadenada<T> list;
	int ultimo;
	
	public Queue() {
		list = new ListaEncadenada<T>();
		ultimo = 0;
	}
	
	public void enqueue(T item)
	{
		
		list.agregarElementoFinal(item);
		
		ultimo++;
		
	}
	
	public T dequeue()
	{
		T tipo = list.darElemento(1);
		list.remove(1);
		ultimo--;
		return tipo;
	}
	
	public boolean isEmpty()
	{
		return ultimo == 0;
	}
	public int size()
	{
		return ultimo;
	}
	
}
