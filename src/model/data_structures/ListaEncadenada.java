package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T extends Comparable<T>> implements ILista<T> {

	private NodoSencillo<T> actual;

	private NodoSencillo<T> primero;

	public ListaEncadenada(){
		primero = null;
		actual = primero;
	}

	@Override
	public Iterator<T> iterator() {			
		// TODO Auto-generated method stub
		return this.actual;
	}


	public boolean estaVacia(){
		return primero == null;
	}


	public void agregarElementoFinal(T elem) {
		NodoSencillo<T> var = new NodoSencillo<>(elem);
		NodoSencillo<T> otro;
		if(darNumeroElementos() == 0){
			primero = var;
			actual = var;
		}
		else{
			otro = actual;
			while(otro.hasNext()){
				otro = otro.next();
			}
			otro.setSiguiente(var);
		}
		// TODO Auto-generated method stub

	}

	@Override
	public T darElemento(int pos) {
		int c=1;
		NodoSencillo<T> r = primero;
		T tipo = null;
		int total = darNumeroElementos();
		if(pos > total){
			System.out.println("La posicion requerida no existe");
		}
		else{
			while(r != null){
				if(c == pos){
					tipo = r.getTipo();
					actual = r;
					break;
				}
				else{
					r = r.next();
					c++;
				}
			}
		}
		return tipo;
	}


	@Override
	public int darNumeroElementos() {
		NodoSencillo<T> var = primero;
		int c=0;
		if(estaVacia()){
			return c;
		}
		else{
			while(var != null){
				c++;
				var = var.next();
			}
		}
		return c;
		// TODO Auto-generated method stub
	}

	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return actual.getTipo();
	}

	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		boolean r= false;
		if(actual.hasNext()){
			actual = actual.next();
			r = true;
		}
		return r;
	}


	public boolean retrocederPosicionAnterior() {
		boolean r = false;
		NodoSencillo<T> var = primero;
		// TODO Auto-generated method stub
		if(actual == primero){
			r = false;
		}
		else{

			while(var != null){
				if(var.next() == actual){
					actual = var;
					r = true;
					break;
				}
				else{
					var = var.next();
				}
			}
		}
		return r;
	}
	
	public void remove(int pos)
	{
		if(pos == 1)
		{
			primero= primero.next();
		}
		else
		{
			darElemento(pos-1);
			if(primero == null)
			{
				primero =actual;
			}
			actual.remove();

		}
	}
	
	

	
	@Override
	public T eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		return primero == null;
	}

	@Override
	public void setElemento(int pos, T item) {
		NodoSencillo<T> var = new NodoSencillo<>(item);
		if(pos <= this.darNumeroElementos()){
			actual = primero;
			for(int i = 0; i < pos; i++){
				avanzarSiguientePosicion();
			}
			actual.setSiguiente(var);
		}
		else{
			agregarElementoFinal(item);
		}	
		
	}
	
	
}
