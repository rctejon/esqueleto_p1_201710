package controller;

import model.data_structures.ILista;
import model.data_structures.Stack;
import model.logic.SistemaRecomendacionPeliculas;

public class Controller {


	private static SistemaRecomendacionPeliculas peliculas= new SistemaRecomendacionPeliculas();

	public void cargarLista(){
		peliculas.cargarPeliculasSR("data/movies.csv");
	}

	public void cargarUsuarios(){
		peliculas.cargarUsuariosSR("data/movies.csv");
	}

	public void cargarRatings(){
		peliculas.cargarRatingsSR("data/ratingss.csv");
	}

	public void cargarTags(){
		peliculas.cargarTagsSR("data/tags.csv");
	}

	public void cargarRecomendaciones(){
		peliculas.cargarRecomendaciones("data/movies.csv");
	}
	
	public void peliculasPolulares(){
		peliculas.peliculasPopularesSR(3);
	}
	
	public void catalogoPeliculasOrdenado(){
		peliculas.catalogoPeliculasOrdenadoSR();
	}
	
	public void recomendarGeneros()
	{
peliculas.recomendarGeneroSR();
	}
	
	public void opinionRatingGen(){
		peliculas.opinionRatingsGeneroSR();
		
	}
	
	public void recomendarPeliculas(){
		peliculas.recomendarPeliculasSR("data/movies.csv", 5);
	}
	
	public void ratingPelicula(){
		peliculas.ratingsPeliculaSR(3);
	}
	
	public void usuariosActivos(){
		peliculas.usuariosActivosSR(4);
	}
	
	public void catalogoUsuario(){
		peliculas.catalogoUsuariosOrdenadoSR();
	}
	
	public void recomendarTagsGen(){
		peliculas.recomendarTagsGeneroSR(3);
	}
	
	public void opinionTagGen(){
		peliculas.opinionTagsGeneroSR();
	}
	
	public void recomendarUsuario(){
		peliculas.recomendarUsuariosSR("data/movies.csv", 3);
	}
	
	public void tagsPelicula(){
		peliculas.tagsPeliculaSR(3);
	}
	
	public void darHistorialOper(){
		peliculas.darHistoralOperacionesSR();
	}

	public void limpiarHistorial(){
		peliculas.limpiarHistorialOperacionesSR();
}
	
	public void darUltimasOper(){
		peliculas.darHistoralOperacionesSR();
	}
	
	public void borrarUltOper(){
		peliculas.borrarUltimasOperaciones(5);
	}
}
