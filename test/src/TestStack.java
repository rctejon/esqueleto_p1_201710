package Tests;

import junit.framework.TestCase;
import model.data.estructures.Stack;

public class TestStack extends TestCase{
	
	private Stack<String> pila;
	
	public void setupEscenario1(){
		pila = new Stack<String>();

		pila.push("Prueba");
		pila.push("Hola");
		pila.push("Millos");
		pila.push("Hey");
		pila.push("Fin");
	}
	
	public void testPushYPop(){
		setupEscenario1();
		System.out.println("1");
		
		assertEquals("No es el elemento esperado", "Fin", pila.pop());
		assertEquals("No es el elemento esperado", "Hey", pila.pop());
		assertEquals("No es el elemento esperado", "Millos", pila.pop());
		assertEquals("No es el elemento esperado", "Hola", pila.pop());
		assertEquals("No es el elemento esperado", "Prueba", pila.pop());
		
		pila.push("Jaja");
		assertEquals("No es el elemento esperado", "Jaja", pila.pop());
	}
	
	public void testIsEmty(){
		setupEscenario1();
		
		assertEquals("No es lo esperado", false, pila.isEmty());
		
		for(int i = 0; i < 6 ; i++){
			pila.pop();
		}
		
		assertEquals("No es lo esperado", true, pila.isEmty());
	}
	
	public void testSize(){
		setupEscenario1();
		
		assertEquals("No es el numero esperado", 5, pila.size());
		
		for(int i = 5; i > 0 ; i--){
			pila.pop();
			assertEquals("No es el numero esperado", i-1, pila.size());
		}
		
	}

}
