import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class TestListaEncadenada extends TestCase{
	
	private ListaEncadenada lista;
	
	public void setupEscenario1(){
		lista = new ListaEncadenada<Number>();
		lista.agregarElementoFinal(1);
		lista.agregarElementoFinal(16);
		lista.agregarElementoFinal(15);
		lista.agregarElementoFinal(2);
		lista.agregarElementoFinal(86);
	}
	
	public void testAgregarElementoFinalYDarElemento(){
		
		setupEscenario1();
		
		assertEquals("No es el numero esperado", 1, lista.darElemento(0));
		assertEquals("No es el numero esperado", 16, lista.darElemento(1));
		assertEquals("No es el numero esperado", 15, lista.darElemento(2));
		assertEquals("No es el numero esperado", 2, lista.darElemento(3));
		assertEquals("No es el numero esperado", 86, lista.darElemento(4));
		
	}
	
	public void testDarNumeroElementos(){
		
		setupEscenario1();
		
		assertEquals("No es la cantidad esperada", 5, lista.darNumeroElementos());
		
		lista.agregarElementoFinal(76);
		assertEquals("No es la cantidad esperada", 6, lista.darNumeroElementos());
		
	}
	
	public void testDarElementoPosicionActual(){
		
		setupEscenario1();
		
		assertEquals("No es el numero esperado", 1, lista.darElementoPosicionActual());
		
		lista.avanzarSiguientePosicion();
		assertEquals("No es el numero esperado", 16, lista.darElementoPosicionActual());
		
		lista.avanzarSiguientePosicion();
		assertEquals("No es el numero esperado", 15, lista.darElementoPosicionActual());
		
		lista.avanzarSiguientePosicion();
		assertEquals("No es el numero esperado", 2, lista.darElementoPosicionActual());
	}
	
	public void testAvanzarPosicion(){
		
		setupEscenario1();
		
		assertEquals("No es lo esperado", true, lista.avanzarSiguientePosicion());
		assertEquals("No es el numero esperado", 16, lista.darElementoPosicionActual());
		
		assertEquals("No es lo esperado", true, lista.avanzarSiguientePosicion());
		assertEquals("No es el numero esperado", 15, lista.darElementoPosicionActual());
		
		for(int i = 0; i<4; i++){
			lista.avanzarSiguientePosicion();
		}
		
		assertEquals("No es lo esperado", false, lista.avanzarSiguientePosicion());
		assertEquals("No es el numero esperado", 86, lista.darElementoPosicionActual());
		
	}

	public void testRetrocederPosicion(){
		
		setupEscenario1();
		
		assertEquals("No es lo esperado", false, lista.retrocederPosicionAnterior());
		assertEquals("No es el numero esperado", 1, lista.darElementoPosicionActual());
		
		for(int i = 0; i<5; i++){
			lista.avanzarSiguientePosicion();
		}
		
		assertEquals("No es lo esperado", true, lista.retrocederPosicionAnterior());
		assertEquals("No es el numero esperado", 2, lista.darElementoPosicionActual());
		
		assertEquals("No es lo esperado", true, lista.retrocederPosicionAnterior());
		assertEquals("No es el numero esperado", 15, lista.darElementoPosicionActual());
		
		
		
	}
}
