package Tests;


import junit.framework.TestCase;
import model.data.estructures.Queue;


public class TestQueue extends TestCase{
	
	private Queue<String> cola;
	
	public void setupEscenario1(){
		cola = new Queue<>();

		cola.enqueue("Prueba");
		cola.enqueue("Hola");
		cola.enqueue("Millos");
		cola.enqueue("Hey");
		cola.enqueue("Fin");
	}
	
	public void testEnqueueYDequeue(){
		setupEscenario1();
		
		assertEquals("No es el elemento esperado", "Prueba", cola.dequeue());
		assertEquals("No es el elemento esperado", "Hola", cola.dequeue());
		assertEquals("No es el elemento esperado", "Millos", cola.dequeue());
		assertEquals("No es el elemento esperado", "Hey", cola.dequeue());
		assertEquals("No es el elemento esperado", "Fin", cola.dequeue());
		
		cola.enqueue("Jaja");
		assertEquals("No es el elemento esperado", "Jaja", cola.dequeue());
	}
	
	public void testIsEmty(){
		setupEscenario1();
		
		assertEquals("No es lo esperado", false, cola.isEmpty());
		
		for(int i = 0; i < 6 ; i++){
			cola.dequeue();
		}
		
		assertEquals("No es lo esperado", true, cola.isEmpty());
	}
	
	public void testSize(){
		setupEscenario1();
		
		assertEquals("No es el numero esperado", 5, cola.size());
		
		for(int i = 5; i > 0 ; i--){
			cola.dequeue();
			assertEquals("No es el numero esperado", i-1, cola.size());
		}
		
	}
	
	
}
